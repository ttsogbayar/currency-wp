
/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function ($, _c, undefined) {
	'use strict';

	// Use this variable to set up the common and page specific functions. If you
	// rename this variable, you will also need to rename the namespace below.

	var Sage = {
		// All pages
		'common': {
			init: function() {
				// JavaScript to be fired on all pages

				// Scroll To
				(function() {
					$('.scrollto').on('click', function(e){
						e.preventDefault();
						var section = $(this).attr('href');
						$('html, body').animate({
				            scrollTop: $(section).offset().top
				        }, 1000);
					});
				})();

				// Lightbox
				(function() {
					$('.form-btn').on('click', function(e){
						e.preventDefault();
						$('.lightbox').fadeIn(600);

						particlesJS("dust-particles2", {
						  "particles": {
						    "number": {
						      "value": 20,
						      "density": {
						        "enable": false,
						        "value_area": 800
						      }
						    },
						    "color": {
						      "value": "#36edff"
						    },
						    "shape": {
						      "type": "circle",
						      "stroke": {
						        "width": 0,
						        "color": "#000000"
						      },
						      "polygon": {
						        "nb_sides": 5
						      },
						      "image": {
						        "src": "img/github.svg",
						        "width": 100,
						        "height": 100
						      }
						    },
						    "opacity": {
						      "value": 1,
						      "random": true,
						      "anim": {
						        "enable": false,
						        "speed": 1,
						        "opacity_min": 0.1,
						        "sync": false
						      }
						    },
						    "size": {
						      "value": 10,
						      "random": true,
						      "anim": {
						        "enable": false,
						        "speed": 40,
						        "size_min": 0.1,
						        "sync": false
						      }
						    },
						    "line_linked": {
						      "enable": false,
						      "distance": 150,
						      "color": "#ffffff",
						      "opacity": 0.4,
						      "width": 1
						    },
						    "move": {
						      "enable": true,
						      "speed": 2,
						      "direction": "none",
						      "random": false,
						      "straight": false,
						      "out_mode": "out",
						      "bounce": false,
						      "attract": {
						        "enable": false,
						        "rotateX": 600,
						        "rotateY": 1200
						      }
						    }
						  },
						  "interactivity": {
						    "detect_on": "canvas",
						    "events": {
						      "onhover": {
						        "enable": false,
						        "mode": "repulse"
						      },
						      "onclick": {
						        "enable": false,
						        "mode": "push"
						      },
						      "resize": true
						    },
						    "modes": {
						      "grab": {
						        "distance": 400,
						        "line_linked": {
						          "opacity": 1
						        }
						      },
						      "bubble": {
						        "distance": 400,
						        "size": 40,
						        "duration": 2,
						        "opacity": 8,
						        "speed": 3
						      },
						      "repulse": {
						        "distance": 200,
						        "duration": 0.4
						      },
						      "push": {
						        "particles_nb": 4
						      },
						      "remove": {
						        "particles_nb": 2
						      }
						    }
						  },
						  "retina_detect": true
						});						
					});
					$('.lightbox .close, .lightbox .close-link').on('click', function(e){
						e.preventDefault();
						$('.lightbox').fadeOut(600);
					});
				})();

			},
			finalize: function() {
				// JavaScript to be fired on all pages, after page specific JS is fired
			}
		},

		// Home page
		'page_template_template_home': {
			init: function() {


				// on load
				$(window).load(function(){
					$('#hero .dot1').addClass('on');
					$('#hero .dot2').addClass('on');
					$('#hero .dot3').addClass('on');
					$('#hero .dot4').addClass('on');
					$('#hero .photo').addClass('on');
					$('#hero .link').addClass('on');
					$('#hero .cnt h1').addClass('on');
					$('#hero .cnt p').addClass('on');
					$('#hero .link').addClass('on');
					setTimeout(function(){
						$('.form-btn').addClass('on');
						$('.sticky-nav').addClass('on');
						$('#hero .hero-scroll').addClass('on');
						$('#hero .hero-scroll div').addClass('rotate');
						$('.page-view').addClass('on');
						$('.footer-nav').addClass('on');
					}, 1300);
				});


				// switch view
				$('.full-page').on('click', function(e){
					e.preventDefault();
					$('#col-page').fadeOut(100);
					$('#full-page').fadeIn(300);
					$('.sticky-nav').removeClass('off');
					$('.col-page').removeClass('active');
					$('.float-dot1').show();
					$('.float-dot2').show();
					$(this).addClass('active');
					$('#hero .hero-scroll').attr('href', '#section1');
				});

				$('.col-page').on('click', function(e){
					e.preventDefault();
					$('#full-page').fadeOut(100);
					$('#col-page').fadeIn(300);
					$('.sticky-nav').addClass('off');
					$('.full-page').removeClass('active');
					$('.float-dot1').hide();
					$('.float-dot2').hide();
					$(this).addClass('active');
					$('#hero .hero-scroll').attr('href', '#col-page');
					$('html, body').animate({
			            scrollTop: $('#col-page').offset().top
			        }, 1000);
				});


				// waypoints
				$('.section').each(function(e){
					var section = $(this);
					var section_waypoint_down = new Waypoint({
						element: section[0],
						handler: function(direction) {
							if (direction === 'down'){
								$(section).addClass('on');
						    	$('.sticky-nav li').removeClass('active');
						    	$('.sticky-nav .section-'+e).addClass('active');
							}
						},
						offset: '100%'
					});
					var section_waypoint_up = new Waypoint({
						element: section[0],
						handler: function(direction) {
							if (direction === 'up'){
						    	$('.sticky-nav li').removeClass('active');
						    	$('.sticky-nav .section-'+e).addClass('active');
							}
						},
						offset: '23%'
					});					
				});

				var waypoint_nav_down = new Waypoint({
					element: document.getElementById('section6'),
					handler: function(direction) {
						if (direction === 'down'){
							$('.sticky-nav').addClass('off');
						}
					},
					offset: '23%'
				});

				var waypoint_nav_up = new Waypoint({
					element: document.getElementById('section6'),
					handler: function(direction) {
						if (direction === 'up'){
							$('.sticky-nav').removeClass('off');
						}
					},
					offset: '23%'
				});

				var waypoint_nav_up2 = new Waypoint({
					element: document.getElementById('section4'),
					handler: function(direction) {
						if (direction === 'up'){
							$('.sticky-nav').removeClass('off');
						}
					},
					offset: '23%'
				});

				var waypoint_section1_menu = new Waypoint({
					element: document.getElementById('full-page'),
					handler: function(direction) {
						if (direction === 'down'){
							var link_width = $('.footer-nav li.section1-link').width();
							$('.footer-nav li').removeClass('active');
							$('.footer-nav li.section1-link').addClass('active');
							$('.menu-line').css({'left': '0', 'width': link_width});
						}
						if (direction === 'up'){
							$('.menu-line').css({'left': '0', 'width': '0'});
							$('.footer-nav li.section1-link').removeClass('active');
						}
					},
					offset: '50%'
				});

				var waypoint_section5_menu = new Waypoint({
					element: document.getElementById('section5'),
					handler: function(direction) {
						if (direction === 'down'){
							var link_width = $('.footer-nav li.section5-link').width();
							var link_left = $('.footer-nav li.section1-link').width()+30;
							$('.footer-nav li').removeClass('active');
							$('.footer-nav li.section5-link').addClass('active');
							$('.menu-line').css({'left': link_left, 'width': link_width});
						}
						if (direction === 'up'){
							var link_width = $('.footer-nav li.section1-link').width();
							$('.footer-nav li').removeClass('active');
							$('.footer-nav li.section1-link').addClass('active');
							$('.menu-line').css({'left': '0', 'width': link_width});
						}
					},
					offset: '50%'
				});

				var waypoint_section6_menu = new Waypoint({
					element: document.getElementById('section6'),
					handler: function(direction) {
						if (direction === 'down'){
							var link_width = $('.footer-nav li.section6-link').width();
							var link_left = $('.footer-nav li.section5-link').width()+30;
							var link_left2 = $('.footer-nav li.section1-link').width()+30;
							var link_left3 = link_left + link_left2;
							$('.footer-nav li').removeClass('active');
							$('.footer-nav li.section6-link').addClass('active');
							$('.menu-line').css({'left': link_left3, 'width': link_width});
						}
						if (direction === 'up'){
							var link_width = $('.footer-nav li.section5-link').width();
							var link_left = $('.footer-nav li.section1-link').width()+30;
							$('.footer-nav li').removeClass('active');
							$('.footer-nav li.section5-link').addClass('active');
							$('.menu-line').css({'left': link_left, 'width': link_width});
						}
					},
					offset: '50%'
				});


				// footer links
				$('.footer-nav li').each(function(){
					if ($(this).hasClass('active')){
						if ($(this).hasClass('section1-link')){
							var set_left = $(this).position.left;
						} else {
							var set_left = $(this).position.left+30;
						}
						var set_width = $(this).width();
						$('.menu-line').css({'left': set_left, 'width': set_width});
					}
				});


				// scroll magic
                var scrollMagicController = new ScrollMagic.Controller();

                $('#hero').each(function () {
                    var section = $(this).attr('id');
                    var section_height = $('#hero').height() * 6;
                    var tween_left = TweenMax.staggerFromTo(".left-gray", 1, {left: '15%'}, {
                        left: '-100%',
                    });
                    var scene_left = new ScrollMagic.Scene({
                        triggerElement: '#hero',
                        duration: section_height,
                    }).setTween(tween_left).addTo(scrollMagicController);

                    var tween_right = TweenMax.staggerFromTo(".right-gray", 1, {right: '15%'}, {
                        right: '-100%',
                    });
                    var scene_right = new ScrollMagic.Scene({
                        triggerElement: '#hero',
                        duration: section_height,
                    }).setTween(tween_right).addTo(scrollMagicController);
                });

				$('.split-section').each(function () {
                    var section = $(this).attr('id');
                    var tween_top = TweenMax.fromTo("#"+section+" .content", 1,
                    	{top: '120px'},
                    	{top: '520px'}
                    );
                    var scene_top = new ScrollMagic.Scene({
                        triggerElement: '#'+section,
                        duration: 2000,
                        reverse: true,
                    }).setTween(tween_top).addTo(scrollMagicController);
                });


				// dots
				(function() {

					if ($(window).width() > 1023){
						var duration1 = 780;
						var duration2 = 550;
					}
					if ($(window).width() < 1023){
						var duration1 = 620;
						var duration2 = 550;
					}

					// section 1 dots
	                var section1_offset = $('#section1').offset().top;
	                var section1_top = $('#section1').height() / 2;
	                var section1_duration = section1_offset + section1_top;

	                if ($(window).width() > 1023){
		                // dot 1
		                var tween_section1_dot1 = TweenMax.fromTo(".float-dot1", 1,
		                	{top: '880px', left: '-400px'},
		                	{top: section1_duration, left: '350px'}
		                );
		                var scene_section1_dot1 = new ScrollMagic.Scene({
		                    triggerElement: '#section1',
		                    duration: duration1,
		                    reverse: true,
		                }).setTween(tween_section1_dot1).addTo(scrollMagicController);

		                // dot 2
		                var tween_section1_dot2 = TweenMax.fromTo(".float-dot2", 1,
		                	{top: '880px', left: '-400px'},
		                	{top: section1_duration, left: '160px'}
		                );
		                var scene_section1_dot2 = new ScrollMagic.Scene({
		                    triggerElement: '#section1',
		                    duration: duration2,
		                    reverse: true,
		                }).setTween(tween_section1_dot2).addTo(scrollMagicController);
		            }

	                if ($(window).width() < 1023){
		                // dot 1
		                var tween_section1_dot1 = TweenMax.fromTo(".float-dot1", 1,
		                	{top: '880px', left: '-400px'},
		                	{top: section1_duration, left: '150px'}
		                );
		                var scene_section1_dot1 = new ScrollMagic.Scene({
		                    triggerElement: '#section1',
		                    duration: duration1,
		                    reverse: true,
		                }).setTween(tween_section1_dot1).addTo(scrollMagicController);

		                // dot 2
		                var tween_section1_dot2 = TweenMax.fromTo(".float-dot2", 1,
		                	{top: '880px', left: '-400px'},
		                	{top: section1_duration, left: '60px'}
		                );
		                var scene_section1_dot2 = new ScrollMagic.Scene({
		                    triggerElement: '#section1',
		                    duration: duration2,
		                    reverse: true,
		                }).setTween(tween_section1_dot2).addTo(scrollMagicController);
		            }

					// section 2 dots
	                var section2_offset = $('#section2').offset().top;
	                var section2_top = $('#section2').height() / 2;
	                var section2_duration = section2_offset + section2_top;

	                if ($(window).width() > 1023){
	                	// dot 1
		                var tween_section2_dot1 = TweenMax.fromTo(".float-dot1", 1,
		                	{top: section1_duration, left: '350px'},
		                	{top: section2_duration, left: '940px'}
		                );
		                var scene_section2_dot1 = new ScrollMagic.Scene({
		                    triggerElement: '#section2',
		                    duration: duration1,
		                    reverse: true,
		                }).setTween(tween_section2_dot1).addTo(scrollMagicController);

		                // dot 2
		                var tween_section2_dot2 = TweenMax.fromTo(".float-dot2", 1,
		                	{top: section1_duration, left: '160px'},
		                	{top: section2_duration, left: '740px'}
		                );
		                var scene_section2_dot2 = new ScrollMagic.Scene({
		                    triggerElement: '#section2',
		                    duration: duration2,
		                    reverse: true,
		                }).setTween(tween_section2_dot2).addTo(scrollMagicController);
	            	}

	                if ($(window).width() < 1023){
	                	// dot 1
		                var tween_section2_dot1 = TweenMax.fromTo(".float-dot1", 1,
		                	{top: section1_duration, left: '150px'},
		                	{top: section2_duration, left: '440px'}
		                );
		                var scene_section2_dot1 = new ScrollMagic.Scene({
		                    triggerElement: '#section2',
		                    duration: duration1,
		                    reverse: true,
		                }).setTween(tween_section2_dot1).addTo(scrollMagicController);

		                // dot 2
		                var tween_section2_dot2 = TweenMax.fromTo(".float-dot2", 1,
		                	{top: section1_duration, left: '60px'},
		                	{top: section2_duration, left: '840px'}
		                );
		                var scene_section2_dot2 = new ScrollMagic.Scene({
		                    triggerElement: '#section2',
		                    duration: duration2,
		                    reverse: true,
		                }).setTween(tween_section2_dot2).addTo(scrollMagicController);
	            	}

					// section 3 dots
	                var section3_offset = $('#section3').offset().top;
	                var section3_top = $('#section3').height() / 2;
	                var section3_duration = section3_offset + section3_top;

	                if ($(window).width() > 1023){
	                	// dot 1
		                var tween_section3_dot1 = TweenMax.fromTo(".float-dot1", 1,
		                	{top: section2_duration, left: '940px'},
		                	{top: section3_duration, left: '-40px'}
		                );
		                var scene_section3_dot1 = new ScrollMagic.Scene({
		                    triggerElement: '#section3',
		                    duration: duration1,
		                    reverse: true,
		                }).setTween(tween_section3_dot1).addTo(scrollMagicController);

		                // dot 2
		                var tween_section3_dot2 = TweenMax.fromTo(".float-dot2", 1,
		                	{top: section2_duration, left: '740px'},
		                	{top: section3_duration, left: '500px'}
		                );
		                var scene_section3_dot2 = new ScrollMagic.Scene({
		                    triggerElement: '#section3',
		                    duration: duration2,
		                    reverse: true,
		                }).setTween(tween_section3_dot2).addTo(scrollMagicController);
		            }

	                if ($(window).width() < 1023){
	                	// dot 1
		                var tween_section3_dot1 = TweenMax.fromTo(".float-dot1", 1,
		                	{top: section2_duration, left: '440px'},
		                	{top: section3_duration, left: '140px'}
		                );
		                var scene_section3_dot1 = new ScrollMagic.Scene({
		                    triggerElement: '#section3',
		                    duration: duration1,
		                    reverse: true,
		                }).setTween(tween_section3_dot1).addTo(scrollMagicController);

		                // dot 2
		                var tween_section3_dot2 = TweenMax.fromTo(".float-dot2", 1,
		                	{top: section2_duration, left: '840px'},
		                	{top: section3_duration, left: '100px'}
		                );
		                var scene_section3_dot2 = new ScrollMagic.Scene({
		                    triggerElement: '#section3',
		                    duration: duration2,
		                    reverse: true,
		                }).setTween(tween_section3_dot2).addTo(scrollMagicController);
		            }

					// section 4 dots
	                var section4_offset = $('#section4').offset().top;
	                var section4_top = $('#section4').height() / 2;
	                var section4_duration = section4_offset + section4_top;

	                if ($(window).width() > 1023){
	                	// dot 1
		                var tween_section4_dot1 = TweenMax.fromTo(".float-dot1", 1,
		                	{top: section3_duration, left: '-40px'},
		                	{top: section4_duration, left: '980px'}
		                );
		                var scene_section4_dot1 = new ScrollMagic.Scene({
		                    triggerElement: '#section4',
		                    duration: duration1,
		                    reverse: true,
		                }).setTween(tween_section4_dot1).addTo(scrollMagicController);

		                // dot 2
		                var tween_section4_dot2 = TweenMax.fromTo(".float-dot2", 1,
		                	{top: section3_duration, left: '500px'},
		                	{top: section4_duration, left: '780px'}
		                );
		                var scene_section4_dot2 = new ScrollMagic.Scene({
		                    triggerElement: '#section4',
		                    duration: duration2,
		                    reverse: true,
		                }).setTween(tween_section4_dot2).addTo(scrollMagicController);
		            }

	                if ($(window).width() < 1023){
	                	// dot 1
		                var tween_section4_dot1 = TweenMax.fromTo(".float-dot1", 1,
		                	{top: section3_duration, left: '140px'},
		                	{top: section4_duration, left: '480px'}
		                );
		                var scene_section4_dot1 = new ScrollMagic.Scene({
		                    triggerElement: '#section4',
		                    duration: duration1,
		                    reverse: true,
		                }).setTween(tween_section4_dot1).addTo(scrollMagicController);

		                // dot 2
		                var tween_section4_dot2 = TweenMax.fromTo(".float-dot2", 1,
		                	{top: section3_duration, left: '100px'},
		                	{top: section4_duration, left: '780px'}
		                );
		                var scene_section4_dot2 = new ScrollMagic.Scene({
		                    triggerElement: '#section4',
		                    duration: duration2,
		                    reverse: true,
		                }).setTween(tween_section4_dot2).addTo(scrollMagicController);
		            }
	            })();


	            // map
				function initialize(){
					var styles=[{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#000000"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":17},{"weight":1.2}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":17}]}];
				    var styledMap = new google.maps.StyledMapType(styles, {name: "tehgrayz"});
				    var mapOptions = {
				        center: new google.maps.LatLng(41.8935778, -87.6259213),
				        zoom: 12,
				        mapTypeControlOptions: {
				            mapTypeIds: [google.maps.MapTypeId.ROADMAP, 'map_style']
				        },
				        panControl: false,
				        zoomControl: false,
				        mapTypeControl: false,
				        scaleControl: false,
				        streetViewControl: false,
				        overviewMapControl: false
				    };
				    var map = new google.maps.Map(document.getElementById("map"),
				    mapOptions);
				    var marker = new google.maps.Marker({
				        position: new google.maps.LatLng(41.8935778, -87.6259213),
				        icon: 'http://localhost:8080/images/pin.png',
				    });
				    map.mapTypes.set('map_style', styledMap);
				    map.setMapTypeId('map_style');
				    marker.setMap(map);
				}
				initialize();


				// dust particles
				particlesJS("dust-particles", {
				  "particles": {
				    "number": {
				      "value": 20,
				      "density": {
				        "enable": false,
				        "value_area": 800
				      }
				    },
				    "color": {
				      "value": "#36edff"
				    },
				    "shape": {
				      "type": "circle",
				      "stroke": {
				        "width": 0,
				        "color": "#000000"
				      },
				      "polygon": {
				        "nb_sides": 5
				      },
				      "image": {
				        "src": "img/github.svg",
				        "width": 100,
				        "height": 100
				      }
				    },
				    "opacity": {
				      "value": 1,
				      "random": true,
				      "anim": {
				        "enable": false,
				        "speed": 1,
				        "opacity_min": 0.1,
				        "sync": false
				      }
				    },
				    "size": {
				      "value": 10,
				      "random": true,
				      "anim": {
				        "enable": false,
				        "speed": 40,
				        "size_min": 0.1,
				        "sync": false
				      }
				    },
				    "line_linked": {
				      "enable": false,
				      "distance": 150,
				      "color": "#ffffff",
				      "opacity": 0.4,
				      "width": 1
				    },
				    "move": {
				      "enable": true,
				      "speed": 2,
				      "direction": "none",
				      "random": false,
				      "straight": false,
				      "out_mode": "out",
				      "bounce": false,
				      "attract": {
				        "enable": false,
				        "rotateX": 600,
				        "rotateY": 1200
				      }
				    }
				  },
				  "interactivity": {
				    "detect_on": "canvas",
				    "events": {
				      "onhover": {
				        "enable": false,
				        "mode": "repulse"
				      },
				      "onclick": {
				        "enable": false,
				        "mode": "push"
				      },
				      "resize": true
				    },
				    "modes": {
				      "grab": {
				        "distance": 400,
				        "line_linked": {
				          "opacity": 1
				        }
				      },
				      "bubble": {
				        "distance": 400,
				        "size": 40,
				        "duration": 2,
				        "opacity": 8,
				        "speed": 3
				      },
				      "repulse": {
				        "distance": 200,
				        "duration": 0.4
				      },
				      "push": {
				        "particles_nb": 4
				      },
				      "remove": {
				        "particles_nb": 2
				      }
				    }
				  },
				  "retina_detect": true
				});

			},
			finalize: function() {
				// JavaScript to be fired on the home page, after the init JS
			}
		},

	};

	// The routing fires all common scripts, followed by the page specific scripts.
	// Add additional events for more control over timing e.g. a finalize event
	var UTIL = {
		fire: function(func, funcname, args) {
			var fire;
			var namespace = Sage;
			funcname = funcname === undefined ? 'init' : funcname;
			fire = func !== '';
			fire = fire && namespace[func];
			fire = fire && typeof namespace[func][funcname] === 'function';

			if (fire) {
				namespace[func][funcname](args);
			}
		},
		loadEvents: function() {
			// Fire common init JS
			UTIL.fire('common');

			// Fire page-specific init JS, and then finalize JS
			$.each(document.body.className.replace(/-/g, '_').split(/\s+/), function (i, classnm) {
				UTIL.fire(classnm);
				UTIL.fire(classnm, 'finalize');
			});

			// Fire common finalize JS
			UTIL.fire('common', 'finalize');
		}
	};

	// Load Events
	$(document).ready(UTIL.loadEvents);
})(window.jQuery, window.Clique); // Fully reference jQuery after this point.
