<?php
/**
 * Template Name: Home
 */
?>

<?php while (have_posts()) : the_post(); ?>
<main class="main" id="main">

	<div class="left-gray"></div>
	<div class="right-gray"></div>

	<div class="float-dot1">&middot;</div>
	<div class="float-dot2">&middot;</div>

	<section id="hero" class="section">
		<div class="wrapper container">
			<div class="dot1"></div>
			<div class="dot2"></div>
			<div class="cnt">
				<h1>Currency</h1>
				<p>Chicago's FinTech Center of Excellence</p>
			</div>
			<div class="dot3"></div>
			<div class="dot4"></div>
			<div class="photo" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/photo1.jpg);"></div>
			<a href="#section1" class="scrollto link"><span>See the Member Benefits</span></a>
			<a href="#section1" class="scrollto hero-scroll">
				<div class="ring1"></div>
				<div class="ring2"></div>
				<div class="ring3"></div>
				<div class="ring4"></div>
				<div class="ring5"></div>
				<span class="arrow"></span>
			</a>
		</div>
	</section>

	<div id="col-page">

		<p class="title">Member Benefits</p>
		<div class="wrapper container">
			<div class="row">
				<div class="col-xs-3">
					<div class="benefit">
						<h2>Space Rental</h2>
						<div class="thumb" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/thumb1.jpg);"></div>
						<div class="copy">
							<h3>Lorem Ipsum Quam Eu Blandit</h3>
							<p>Fusce nec est eros. Ut eleifend quam eu blandit vehicula. In ornare placerat sapien, vel bibendum ipsum. Vivamus at dui nibh. Pellentesque tristique sit amet nibh congue porttitor. Duis hendrerit efficitur magna, eget.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="benefit">
						<h2>Programs</h2>
						<div class="thumb" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/thumb2.jpg);"></div>
						<div class="copy">
							<h3>Lorem Ipsum Quam Eu Blandit</h3>
							<p>Fusce nec est eros. Ut eleifend quam eu blandit vehicula. In ornare placerat sapien, vel bibendum ipsum. Vivamus at dui nibh. Pellentesque tristique sit amet nibh congue porttitor. Duis hendrerit efficitur magna, eget.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="benefit">
						<h2>Mentor Match</h2>
						<div class="thumb" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/thumb3.jpg);"></div>
						<div class="copy">
							<h3>Lorem Ipsum Quam Eu Blandit</h3>
							<p>Fusce nec est eros. Ut eleifend quam eu blandit vehicula. In ornare placerat sapien, vel bibendum ipsum. Vivamus at dui nibh. Pellentesque tristique sit amet nibh congue porttitor. Duis hendrerit efficitur magna, eget.</p>
						</div>
					</div>
				</div>
				<div class="col-xs-3">
					<div class="benefit">
						<h2>Express Meeting</h2>
						<div class="thumb" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/thumb4.jpg);"></div>
						<div class="copy">
							<h3>Lorem Ipsum Quam Eu Blandit</h3>
							<p>Fusce nec est eros. Ut eleifend quam eu blandit vehicula. In ornare placerat sapien, vel bibendum ipsum. Vivamus at dui nibh. Pellentesque tristique sit amet nibh congue porttitor. Duis hendrerit efficitur magna, eget.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<a href="#section5" class="scrollto scroll-btn">
			<div class="ring"></div>
			<span class="arrow"></span>
		</a>
	</div>

	<div id="full-page">
		<section id="section1" class="section split-section">
			<div class="wrapper container">
				<div class="row">
					<div class="col-xs-6">
						<div class="photo" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/photo2.jpg);"></div>
					</div>
					<div class="col-xs-6">
						<div class="content">
							<div class="number">01 -</div>
							<h1>Research</h1>
							<h2>Lorem Ipsum Quam Eu Blandit</h2>
							<p>Fusce nec est eros. Ut eleifend quam eu blandit vehicula. In ornare placerat sapien, vel bibendum ipsum. Vivamus at dui nibh. Pellentesque tristique sit amet nibh congue porttitor. Duis hendrerit efficitur magna, eget.</p>
						</div>
					</div>				
				</div>
			</div>
			<a href="#section2" class="scrollto scroll-btn">
				<div class="ring"></div>
				<span class="arrow"></span>
			</a>
		</section>

		<section id="section2" class="section split-section">
			<div class="wrapper container">
				<div class="row">
					<div class="col-xs-6">
						<div class="content">
							<div class="number">02 -</div>
							<h1>Programs</h1>
							<h2>Lorem Ipsum Quam Eu Blandit</h2>
							<p>Fusce nec est eros. Ut eleifend quam eu blandit vehicula. In ornare placerat sapien, vel bibendum ipsum. Vivamus at dui nibh. Pellentesque tristique sit amet nibh congue porttitor. Duis hendrerit efficitur magna, eget.</p>
						</div>
					</div>				
					<div class="col-xs-6">
						<div class="photo" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/photo3.jpg);"></div>
					</div>
				</div>
			</div>
			<a href="#section3" class="scrollto scroll-btn">
				<div class="ring"></div>
				<span class="arrow"></span>
			</a>
		</section>

		<section id="section3" class="section split-section">
			<div class="wrapper container">
				<div class="row">
					<div class="col-xs-6">
						<div class="photo" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/photo4.jpg);"></div>
					</div>
					<div class="col-xs-6">
						<div class="content">
							<div class="number">03 -</div>
							<h1 class="indent">Mentor Match</h1>
							<h2>Lorem Ipsum Quam Eu Blandit</h2>
							<p>Fusce nec est eros. Ut eleifend quam eu blandit vehicula. In ornare placerat sapien, vel bibendum ipsum. Vivamus at dui nibh. Pellentesque tristique sit amet nibh congue porttitor. Duis hendrerit efficitur magna, eget.</p>
						</div>
					</div>				
				</div>
			</div>
			<a href="#section4" class="scrollto scroll-btn">
				<div class="ring"></div>
				<span class="arrow"></span>
			</a>
		</section>

		<section id="section4" class="section split-section">
			<div class="wrapper container">
				<div class="row">
					<div class="col-xs-6">
						<div class="content">
							<div class="number">04 -</div>
							<h1>Express Meeting</h1>
							<h2>Lorem Ipsum Quam Eu Blandit</h2>
							<p class="half">Fusce nec est eros. Ut eleifend quam eu blandit vehicula. In ornare placerat sapien, vel bibendum ipsum. Vivamus at dui nibh. Pellentesque tristique sit amet nibh congue porttitor. Duis hendrerit efficitur magna, eget.</p>
						</div>
					</div>				
					<div class="col-xs-6">
						<div class="photo" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/photo5.jpg);"></div>
					</div>
				</div>
			</div>
			<a href="#section5" class="scrollto scroll-btn">
				<div class="ring"></div>
				<span class="arrow"></span>
			</a>
		</section>
	</div>

	<section id="section5" class="section location-section">
		<div class="row">
			<div class="col-xs-6">
				<div class="location">
					<div class="cnt">
						<h1>Location</h1>
						<p class="title">Address</p>
						<p>1234 Main Street<br />Chicago, IL 60605</p>
						<p class="title">Phone</p>
						<p>(312) 234-4567</p>
						<ul class="social">
							<li class="twitter"><a href="#" target="_blank">Twitter</a></li>
							<li class="facebook"><a href="#" target="_blank">Facebook</a></li>
							<li class="instagram"><a href="#" target="_blank">Instagram</a></li>
							<li class="linkedin"><a href="#" target="_blank">Linkedin</a></li>
						</ul>
					</div>
				</div>
			</div>				
			<div class="col-xs-6">
				<div class="map-cnt">
					<div id="map"></div>
				</div>
			</div>
		</div>
		<div class="no-location" style="background-image:url(<?= get_stylesheet_directory_uri(); ?>/dist/images/no-location.jpg);"><span>Coming Soon To Chicago</span></div>
	</section>
	
	<section id="section6" class="section contact-section">
		<div class="wrapper container">
			<div class="row center-xs">
				<div class="col-xs-8">
					<p class="title">Contact Us</p>
					<h1>Want to Get Involved?</h1>

<div class="gform_wrapper">
	<a class="gform_anchor"></a>
	<form action="#" enctype="multipart/form-data" method="post" name="gform">
		<div class="validation_error">There was a problem with your submission. Errors have been highlighted below.</div>
		<div class="gform_body">
			<ul class="gform_fields top_label form_sublabel_below description_below">
				<li class="gfield field_sublabel_below field_description_below">
					<label class="gfield_label" for="input_1">Full Name</label>
					<div class="ginput_container ginput_container_text">
						<input class="medium" name="input_1" tabindex="1" type="text" value="" placeholder="Full Name">
					</div>
				</li>
				<li class="gfield field_sublabel_below field_description_below">
					<label class="gfield_label" for="input_2">Email</label>
					<div class="ginput_container ginput_container_email">
						<input class="medium error" name="input_2" tabindex="2" type="email" value="" placeholder="Email">
					</div>
					<div class="gfield_description validation_message">Required field</div>
				</li>
				<li class="gfield field_sublabel_below field_description_below">
					<label class="gfield_label" for="input_1">Phone Number</label>
					<div class="ginput_container ginput_container_text">
						<input class="medium" name="input_1" tabindex="1" type="text" value="" placeholder="Phone Number">
					</div>
				</li>
				<li class="gfield field_sublabel_below field_description_below">
					<label class="gfield_label" for="input_2">Company</label>
					<div class="ginput_container ginput_container_email">
						<input class="medium" name="input_2" tabindex="2" type="text" value="" placeholder="Company">
					</div>
				</li>
				<li class="gfield field_sublabel_below field_description_below">
					<label class="gfield_label" for="input_4">Areas of Interest</label>
					<div class="ginput_container ginput_container_select select-wrap">
						<select class="medium gfield_select" name="input_4" tabindex="4">
							<option value="Select Option">
								Select Option
							</option>
							<option value="Select Option">
								Select Option
							</option>
							<option value="Select Option">
								Select Option
							</option>
							<option value="Select Option">
								Select Option
							</option>
							<option value="Select Option">
								Select Option
							</option>
							<option value="Select Option">
								Select Option
							</option>
						</select>
					</div>
				</li>
				<li class="gfield field_sublabel_below field_description_below">
					<label class="gfield_label" for="input_5">Message</label>
					<div class="ginput_container ginput_container_textarea">
						<textarea class="textarea medium" cols="50" name="input_5" rows="10" tabindex="5" placeholder="Message"></textarea>
					</div>
				</li>				
			</ul>
		</div>
		<div class="gform_footer top_label">
			<input class="gform_button button" onclick="if(window[&quot;gf_submitting&quot;]){return false;} window[&quot;gf_submitting&quot;]=true;" tabindex="6" type="submit" value="Submit">
			<input name="gform_ajax" type="hidden" value="form_id=3&amp;title=&amp;description=&amp;tabindex=1">
			<input class="gform_hidden" name="is_submit" type="hidden" value="1"> <input class="gform_hidden" name="gform_submit" type="hidden" value="3">
			<input class="gform_hidden" name="gform_unique_id" type="hidden" value=""> <input class="gform_hidden" name="state" type="hidden" value="WyJbXSIsIjVlMGQ5N2Y2NGM3Njc5MzZhZTQ4NGEzNDA2NmFjNGE1Il0=">
			<input class="gform_hidden" name="gform_target_page_number" type="hidden" value="0">
			<input class="gform_hidden" name="gform_source_page_number" type="hidden" value="1">
			<input name="gform_field_values" type="hidden" value="">
		</div>
	</form>
</div>
				<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
				</div>				
			</div>
		</div>
		<div id="dust-particles" class="dust"></div>
	</section>
	<p class="copyright">&copy; 2017 Currency. All Rights Reserved. Built by <a href="http://cliquestudios.com/" class="builtby" rel="nofollow" target="_blank">Clique Studios</a>.</p>
</main>

<div class="sticky-nav">
	<ul>
		<li class="section-1 active"><a href="#hero" class="scrollto">00</a></li>
		<li class="section-2"><a href="#section1" class="scrollto">01</a></li>
		<li class="section-3"><a href="#section2" class="scrollto">02</a></li>
		<li class="section-4"><a href="#section3" class="scrollto">03</a></li>
		<li class="section-5"><a href="#section4" class="scrollto">04</a></li>
		<li class="section-6 marker"><a href="#section5" class="scrollto">05</a></li>
	</ul>
</div>

<div id="form-box" class="lightbox">
	<a href="#" class="close"></a>
	<div class="cnt">
		<h2>Stay Up to Date with Our Mailing List</h2>
		<form>
			<input type="text" value="" placeholder="Email Address" />
			<input type="submit" value="Sign Up" />
		</form>
		<a href="#" class="close-link">Close</a>
	</div>
	<div id="dust-particles2" class="dust"></div>
</div>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1v5afIPcnh7aKYrg9quTmubbPjPmwBAM"></script>
<?php endwhile; ?>